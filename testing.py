from pprint import pformat as pp
import random, json, collections, math

STANDARD_DECK = json.load(open('deck.json', 'r'))

def create_deck():
    temp = [value 
            for value in STANDARD_DECK 
            for i in range(STANDARD_DECK[value]['num'])
           ]
    return random.sample(temp, k=len(temp))

def create_hand(deck):
    temp=list()
    for i in range(11+4+4):
        temp.append(deck.pop(0))
        
    return temp

def check_for_pairs(hand, size_goal, num_goal):
    hand_count = collections.Counter(hand)
    
    current_num = 0
    joker_size = hand_count['2'] + hand_count['JOKER']
    
    print(hand_count)  
    print(joker_size)
    
    for card in hand_count:
        if card in ["2", "JOKER"]: continue
        if hand_count[card] < size_goal/2: continue
        if hand_count[card] >= size_goal:
            current_num += 1
        elif hand_count[card] + joker_size >= size_goal:
            current_num += 1
            joker_size -= size_goal - hand_count[card]
       
       
    if current_num >= num_goal:
        return True
    return False

                   

if __name__ == "__main__":
    deck = create_deck()
    hand = create_hand(deck)
    print(check_for_pairs(hand, 4, 2))