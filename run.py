from app import app

app.config.from_object('config.DevelopmentConfig')
app.run(host="localhost", port=app.config['PORT'])
