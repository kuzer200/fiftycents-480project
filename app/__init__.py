from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

app = Flask(__name__)
app.config.from_object('config.Config')

login_manager = LoginManager()
login_manager.init_app(app)
# Must go before controller imports
#db = SQLAlchemy(app)
#db.create_all()

class ItemThing(object):
    def __init__(self):
        self.count=0
    
    def increment(self):
        self.count += 1

thing = ItemThing()

@app.route('/')
def index():
    thing.increment()
    return "Hello! %s" % thing.count
